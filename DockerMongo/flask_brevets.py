"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.caldb
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
############### 

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    # receive datas from AJAX
    km = request.args.get('km', 999, type=float)
    miles = request.args.get('miles', 999, type=float)
    brevet_km = request.args.get('brevet_dist_km', 999, type=int)
    begin_time = request.args.get('begin_time')
    begin_date = request.args.get('begin_date')
    # format the arrow time
    datetime = begin_date + ' ' + begin_time

    # set timezone
    start_date_time = arrow.get(datetime).replace(tzinfo='US/Pacific')
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # package and send data back to AJAX
    open_time, message = acp_times.open_time(km, brevet_km, start_date_time.isoformat())
    close_time = acp_times.close_time(km, brevet_km, start_date_time.isoformat())
    result = {"open": open_time, "close": close_time, "message": message}
    return flask.jsonify(result=result)

@app.route("/_result")
def _result():
    _items = db.caldb.find()
    items = [item for item in _items]
    _distance = db.distance.find()
    distance = [distance for distance in _distance]
    print("inside the _display")
    return render_template('result.html', items=items, distance=distance)
@app.route("/_display")
def _display():
    if db.caldb.count() == 0:
        res = "False"
    else:
        res = "True"
    result = {"res": res}
    return flask.jsonify(result=result)
@app.route("/_submit")
def _submit():
    myargs = request.args.get('res')
    print(myargs == "")
    if myargs == "":
        res = "False"
    else:
        res = "True"
        db.caldb.drop()
        db.distance.drop()
        distance = request.args.get('distance', 999, type=int)
        dict_distance = {}
        dict_distance['distance'] = distance
        db.distance.insert_one(dict_distance)
        array = myargs.split(',')
        array_length = len(array)
        pair_num = int(array_length/4)
        counter = 0
        for index in range(pair_num):
            dic = {}
            dic['mile'] = array[counter]
            counter += 1
            dic['km'] = array[counter]
            counter += 1
            dic['opentime'] = array[counter]
            counter += 1
            dic['closetime'] = array[counter]
            counter += 1
            db.caldb.insert_one(dic)
    result = {"res": res}
    return flask.jsonify(result=result)

#############
app.secret_key = '\xda>u\xba\x8f\xd2[\xdf\x0e\xed\t\xb3\xb5\xc7\xa7t4c$f\xf5\tS\xc6'

if __name__ == "__main__":

    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)
