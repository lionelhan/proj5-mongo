Project 5: Brevet time calculator with Ajax and MongoDB
Simple list of controle times from project 4 stored in MongoDB database.

Author: Qi Han

email: qhan@uoregon.edu

ACP controle times
We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.

How to use the docker to run the server and database

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help. Part of finishing this project is clarifying anything that is not clear about the requirements, and documenting it clearly.

